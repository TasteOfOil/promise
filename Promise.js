console.log("Start");

function Sqrt(num){
    return new Promise((resolve, reject)=>{
        if(num<0){
            reject("Мы не можем вычеслить квадратный корень!");
        }
        else{
            setTimeout(()=>{
                resolve(Math.sqrt(num));
            }
            , 2000);
        }
    })
}

var a = 3;

async function func(){
    try {
        const mes = await Sqrt(a);
        console.log(`sqrt(${a}) = ${mes}`);
    }
    catch(err) {
        console.log(err);
    }
}
func();

console.log("End");